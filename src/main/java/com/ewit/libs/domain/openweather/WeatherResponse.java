package com.ewit.libs.domain.openweather;

import java.util.List;

public class WeatherResponse {

  private WeatherCoordinate coord;
  private List<WeatherDetail> weather;
  private WeatherTemperature main;
  private long dt;
  private long id;
  private String name;

  public WeatherCoordinate getCoord() {
    return coord;
  }

  public void setCoord(WeatherCoordinate coord) {
    this.coord = coord;
  }

  public List<WeatherDetail> getWeather() {
    return weather;
  }

  public void setWeather(List<WeatherDetail> weather) {
    this.weather = weather;
  }

  public WeatherTemperature getMain() {
    return main;
  }

  public void setMain(WeatherTemperature main) {
    this.main = main;
  }

  public long getDt() {
    return dt;
  }

  public void setDt(long dt) {
    this.dt = dt;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
