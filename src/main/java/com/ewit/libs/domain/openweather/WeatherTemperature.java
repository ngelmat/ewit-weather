package com.ewit.libs.domain.openweather;

public class WeatherTemperature {
  private double temp;

  public double getTemp() {
    return temp;
  }

  public void setTemp(double temp) {
    this.temp = temp;
  }
}
