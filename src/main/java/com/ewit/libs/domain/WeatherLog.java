package com.ewit.libs.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ewit.libs.domain.openweather.WeatherResponse;

@Entity
@Table(name = "WEATHER_LOG")
public class WeatherLog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(unique = true)
  private String responseId;

  private String location;
  private String actualWeather;
  private String temperature;
  private Date dtimeInserted;

  public WeatherLog() {}

  public WeatherLog(String location) {
    this.location = location;
  }

  public WeatherLog(WeatherResponse weatherResponse) {
    this.responseId = generateResponseId(weatherResponse);
    this.location = weatherResponse.getName();
    this.actualWeather =
        weatherResponse.getWeather().isEmpty()
            ? "Not Available"
            : weatherResponse.getWeather().get(0).getMain();
    this.temperature = weatherResponse.getMain().getTemp() + " °C";
    this.dtimeInserted = new Date();
  }

  private static String generateResponseId(WeatherResponse weather) {
    StringBuilder responseId = new StringBuilder();
    return responseId
        .append(weather.getId())
        .append(weather.getName())
        .append(weather.getDt())
        .append(weather.getWeather().get(0).getMain())
        .toString();
  }

  public Long getId() {
    return id;
  }

  public String getResponseId() {
    return responseId;
  }

  public String getLocation() {
    return location;
  }

  public String getActualWeather() {
    return actualWeather;
  }

  public String getTemperature() {
    return temperature;
  }

  public Date getDtimeInserted() {
    return dtimeInserted;
  }
}
