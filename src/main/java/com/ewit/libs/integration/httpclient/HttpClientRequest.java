package com.ewit.libs.integration.httpclient;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClientRequest {
  private static CloseableHttpClient httpClient = HttpClients.createDefault();

  public static String get(String url) throws IOException {
    HttpGet get = new HttpGet(url);
    return execute(get);
  }

  // no post request only get

  private static String execute(HttpUriRequest request) throws IOException {
    CloseableHttpResponse response = httpClient.execute(request);
    String rawResponse;
    try {
      rawResponse = EntityUtils.toString(response.getEntity());
    } finally {
      response.close();
    }
    return rawResponse;
  }
}
