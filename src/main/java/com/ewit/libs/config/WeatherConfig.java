package com.ewit.libs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "openweathermap")
public class WeatherConfig {

  private static final String PARAM_API_KEY = "<API_KEY>";
  private static final String PARAM_Q_LOCATION = "<PARAM_LOCATION>";

  private String apiEndpointUrl;
  private String key;

  public String getApiEndpointUrl(String location) {
    return apiEndpointUrl.replace(PARAM_API_KEY, key).replace(PARAM_Q_LOCATION, location);
  }

  public void setApiEndpointUrl(String apiEndpointUrl) {
    this.apiEndpointUrl = apiEndpointUrl;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
