package com.ewit.libs.web.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ewit.libs.service.WeatherService;

@RestController
@RequestMapping("/api/weather")
public class WeatherApiController {

  @Autowired private WeatherService weatherService;

  @RequestMapping("/test")
  public String justTesting() {
    return "test success!";
  }

  @RequestMapping("/getWeather")
  @ResponseBody
  public Object getWeather(@RequestParam("location") String location) throws IOException {
    return weatherService.getWeatherByLocation(location);
  }

  @RequestMapping("/listWeatherLogs")
  @ResponseBody
  public Object listWeatherLogs() {
    return weatherService.listWeatherLogs();
  }
}
