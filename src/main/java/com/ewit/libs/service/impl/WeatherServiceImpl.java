package com.ewit.libs.service.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewit.libs.dao.repository.WeatherRepository;
import com.ewit.libs.domain.WeatherLog;
import com.ewit.libs.domain.openweather.WeatherResponse;
import com.ewit.libs.service.OpenWeatherMapService;
import com.ewit.libs.service.WeatherService;

@Service("weatherService")
public class WeatherServiceImpl implements WeatherService {

  @Autowired private WeatherRepository weatherRepository;
  @Autowired private OpenWeatherMapService openWeatherMapService;

  @Override
  public WeatherLog getWeatherByLocation(String location) throws IOException {
    WeatherResponse weatherResponse = openWeatherMapService.getWeather(location);
    WeatherLog weatherLog = new WeatherLog(weatherResponse);
    saveWeather(weatherLog);

    return weatherLog;
  }

  public void saveWeather(WeatherLog weatherLog) {
    final int MAX_SIZE_UNIQUE_WEATHER_LOG = 5;
    if (weatherRepository.countUniqueResponse(weatherLog.getResponseId()) > 0) {
      return;
    }
    weatherRepository.save(weatherLog);
    List<WeatherLog> dbWeatherLogs = weatherRepository.findWeatherLogByLocation(weatherLog.getLocation());
    List<WeatherLog> weatherLogs =
        dbWeatherLogs.stream()
            .sorted((w1, w2) -> w2.getId().compareTo(w1.getId()))
            .limit(MAX_SIZE_UNIQUE_WEATHER_LOG)
            .collect(Collectors.toList());
    Collections.reverse(weatherLogs);

    WeatherLog deleteWeatherLog = new WeatherLog(weatherLog.getLocation());
    weatherRepository.delete(deleteWeatherLog);
    weatherLogs.stream().forEach((insertWeather) -> weatherRepository.save(insertWeather));
  }

  @Override
  public List<WeatherLog> listWeatherLogs() {
    return (List<WeatherLog>) weatherRepository.findAll();
  }
}
