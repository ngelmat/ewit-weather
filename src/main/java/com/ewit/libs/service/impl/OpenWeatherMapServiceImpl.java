package com.ewit.libs.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ewit.libs.config.WeatherConfig;
import com.ewit.libs.domain.openweather.WeatherResponse;
import com.ewit.libs.integration.httpclient.HttpClientRequest;
import com.ewit.libs.service.OpenWeatherMapService;
import com.google.gson.Gson;

@Service("openWeatherMapService")
public class OpenWeatherMapServiceImpl implements OpenWeatherMapService {
  private static Gson gson = new Gson();

  @Autowired private WeatherConfig config;

  @Override
  public WeatherResponse getWeather(String location) throws IOException {
    String rawResponse = HttpClientRequest.get(config.getApiEndpointUrl(location));
    WeatherResponse weatherResponse = gson.fromJson(rawResponse, WeatherResponse.class);
    return weatherResponse;
  }
}
