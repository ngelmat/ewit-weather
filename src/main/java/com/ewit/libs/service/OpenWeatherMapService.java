package com.ewit.libs.service;

import java.io.IOException;

import com.ewit.libs.domain.openweather.WeatherResponse;

public interface OpenWeatherMapService {

  WeatherResponse getWeather(String location) throws IOException;
}
