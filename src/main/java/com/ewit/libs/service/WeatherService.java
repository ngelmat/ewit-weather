package com.ewit.libs.service;

import java.io.IOException;
import java.util.List;

import com.ewit.libs.domain.WeatherLog;

public interface WeatherService {

  WeatherLog getWeatherByLocation(String location) throws IOException;

  List<WeatherLog> listWeatherLogs();
}
