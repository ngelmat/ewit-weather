package com.ewit.libs.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.ewit.libs.domain.WeatherLog;

@Component
@Repository
public interface WeatherRepository extends CrudRepository<WeatherLog, String> {

  @Query("SELECT w FROM WeatherLog w WHERE w.location = :location ORDER BY id desc")
  List<WeatherLog> findWeatherLogByLocation(@Param("location") String location);

  @Query("SELECT count(id) FROM WeatherLog WHERE response_id = :responseId ")
  int countUniqueResponse(@Param("responseId") String responseId);
}
