package com.ewit.libs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EwitWeatherApplication {

  public static void main(String[] args) {
    SpringApplication.run(EwitWeatherApplication.class, args);
  }

}

